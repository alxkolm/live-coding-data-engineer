import logging
from generators import ticks_generator
from kafka_client import send_message

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__file__)

TOPIC_NAME = 'ticks'

for message in ticks_generator():
    send_message(TOPIC_NAME, message)
