import logging
import sys
import time
from kafka import KafkaProducer
from kafka.errors import KafkaError, NoBrokersAvailable

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__file__)

BROKER_HOST = sys.argv[1] if len(sys.argv) > 1 else 'kafka1:19092'
RETRY_PERIOD = 5

producer = None
connected = False
while not connected:
    try:
        producer = KafkaProducer(
            bootstrap_servers=[BROKER_HOST],
            request_timeout_ms=1000000,
            api_version_auto_timeout_ms=1000000)
        connected = True
    except NoBrokersAvailable as e:
        logger.exception(f"Error connection to {BROKER_HOST}. Retrying in {RETRY_PERIOD}")
        time.sleep(RETRY_PERIOD)

logger.info(f"Successfully connected to {BROKER_HOST}")


def send_message(topic: str, message: str):
    try:
        logger.info('Send message: %s', message)
        result = producer.send(topic, bytes(message, 'utf-8'))
        result.get(timeout=10)
    except KafkaError as e:
        logger.exception("Error while sending message to kafka")
