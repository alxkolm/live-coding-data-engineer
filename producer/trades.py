import logging
from generators import trade_generator
from kafka_client import send_message

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__file__)

TOPIC_NAME = 'trades'

for message in trade_generator():
    send_message(TOPIC_NAME, message)
