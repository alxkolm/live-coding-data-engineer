from datetime import datetime
import random
import ujson
import time
import logging

logger = logging.getLogger(__file__)

SYMBOLS = ['A', 'B', 'C']


def trade_generator():
    seq_id = 1
    while 1:
        trade = {
            'id': seq_id,
            'time': str(datetime.utcnow()),
            'symbol': random.choice(SYMBOLS),
        }
        seq_id += 1
        yield ujson.dumps(trade)
        time.sleep(0.5)


def ticks_generator():
    gauss_mu = 0
    gauss_sigma = 0.005

    seq_id = 1
    prices = {s: 1.0 for s in SYMBOLS}
    while 1:
        # Uncomment next row to get invalid json in stream
        # yield 'invalid json message'

        symbol = random.choice(SYMBOLS)
        # Calculate new price
        prices[symbol] = round(prices[symbol] + random.gauss(gauss_mu, gauss_sigma), 5)
        prices[symbol] = prices[symbol] if 0 < prices[symbol] < 2 else 1.0

        tick = {
            'id': seq_id,
            'time': str(datetime.utcnow()),
            'symbol': symbol,
            'price': prices[symbol],
        }
        seq_id += 1

        # Make random pause
        if random.random() > 0.80:
            duration = random.randrange(2, 5)
            logger.info("Delay ticks producing for %d seconds", duration)
            time.sleep(duration)

        yield ujson.dumps(tick)
        time.sleep(0.1)
