CREATE TABLE ticks
(
    id       integer                                NOT NULL
        CONSTRAINT ticks_pkey
            PRIMARY KEY,
    time     timestamp WITH TIME ZONE               NOT NULL,
    symbol   varchar(32)                            NOT NULL,
    price    double precision                       NOT NULL,
    inserted timestamp WITH TIME ZONE DEFAULT NOW() NOT NULL
);

ALTER TABLE ticks
    OWNER TO postgres;

INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (1, '2021-10-04 06:06:38.369241', 'A', 0.99349, '2021-10-04 09:06:41.360672');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (2, '2021-10-04 06:06:38.696499', 'B', 1.00213, '2021-10-04 09:06:41.366225');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (3, '2021-10-04 06:06:40.802412', 'B', 0.99762, '2021-10-04 09:06:43.819220');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (4, '2021-10-04 06:06:43.914654', 'A', 0.99652, '2021-10-04 09:06:43.930853');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (5, '2021-10-04 06:06:44.026730', 'A', 0.99828, '2021-10-04 09:06:44.042939');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (6, '2021-10-04 06:06:44.138661', 'C', 0.99754, '2021-10-04 09:06:44.155284');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (7, '2021-10-04 06:06:44.250773', 'B', 1.00022, '2021-10-04 09:06:44.265836');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (8, '2021-10-04 06:06:44.361583', 'B', 1.00307, '2021-10-04 09:06:44.375471');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (9, '2021-10-04 06:06:44.472166', 'B', 0.99826, '2021-10-04 09:06:47.490827');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (10, '2021-10-04 06:06:47.584967', 'A', 0.99615, '2021-10-04 09:06:47.599114');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (11, '2021-10-04 06:06:47.695828', 'C', 0.99914, '2021-10-04 09:06:47.712061');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (12, '2021-10-04 06:06:47.808045', 'A', 1.002, '2021-10-04 09:06:47.822746');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (13, '2021-10-04 06:06:47.918153', 'C', 0.99736, '2021-10-04 09:06:49.934618');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (14, '2021-10-04 06:06:50.029309', 'A', 1.00402, '2021-10-04 09:06:50.043731');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (15, '2021-10-04 06:06:50.140040', 'B', 0.98495, '2021-10-04 09:06:50.155645');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (16, '2021-10-04 06:06:50.252145', 'A', 1.00465, '2021-10-04 09:06:54.273722');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (17, '2021-10-04 06:06:54.364811', 'B', 0.97515, '2021-10-04 09:06:58.381560');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (18, '2021-10-04 06:06:58.476754', 'C', 0.99666, '2021-10-04 09:06:58.489533');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (19, '2021-10-04 06:06:58.585443', 'B', 0.96519, '2021-10-04 09:06:58.589704');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (20, '2021-10-04 06:06:58.688490', 'C', 0.99623, '2021-10-04 09:06:58.696674');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (21, '2021-10-04 06:06:58.794304', 'C', 0.99609, '2021-10-04 09:06:58.803009');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (22, '2021-10-04 06:06:58.900330', 'B', 0.96747, '2021-10-04 09:07:00.917200');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (23, '2021-10-04 06:07:01.012540', 'B', 0.97187, '2021-10-04 09:07:01.017840');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (24, '2021-10-04 06:07:01.116622', 'B', 0.9714, '2021-10-04 09:07:01.128957');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (25, '2021-10-04 06:07:01.225212', 'B', 0.96742, '2021-10-04 09:07:01.239471');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (26, '2021-10-04 06:07:01.334762', 'C', 0.99032, '2021-10-04 09:07:04.349296');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (27, '2021-10-04 06:07:04.444911', 'C', 0.99109, '2021-10-04 09:07:04.457321');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (28, '2021-10-04 06:07:04.552917', 'C', 0.98775, '2021-10-04 09:07:04.557353');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (29, '2021-10-04 06:07:04.656513', 'B', 0.96605, '2021-10-04 09:07:04.670777');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (30, '2021-10-04 06:07:04.767075', 'B', 0.96619, '2021-10-04 09:07:08.773145');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (31, '2021-10-04 06:07:08.872146', 'B', 0.96249, '2021-10-04 09:07:11.886228');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (32, '2021-10-04 06:07:11.982777', 'C', 0.98034, '2021-10-04 09:07:11.989445');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (33, '2021-10-04 06:07:12.087307', 'A', 1.00759, '2021-10-04 09:07:12.097827');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (34, '2021-10-04 06:07:12.194969', 'A', 1.01483, '2021-10-04 09:07:12.206176');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (35, '2021-10-04 06:07:12.302727', 'B', 0.95508, '2021-10-04 09:07:12.313629');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (36, '2021-10-04 06:07:12.410361', 'A', 1.00858, '2021-10-04 09:07:12.420324');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (37, '2021-10-04 06:07:12.517773', 'B', 0.9566, '2021-10-04 09:07:16.534035');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (38, '2021-10-04 06:07:16.630589', 'C', 0.98201, '2021-10-04 09:07:16.639718');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (39, '2021-10-04 06:07:16.737172', 'A', 1.0077, '2021-10-04 09:07:20.752416');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (40, '2021-10-04 06:07:20.848303', 'B', 0.94897, '2021-10-04 09:07:20.859377');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (41, '2021-10-04 06:07:20.956644', 'C', 0.9824, '2021-10-04 09:07:20.967772');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (42, '2021-10-04 06:07:21.065027', 'A', 1.01041, '2021-10-04 09:07:21.076558');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (43, '2021-10-04 06:07:21.173377', 'C', 0.97802, '2021-10-04 09:07:21.183882');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (44, '2021-10-04 06:07:21.280832', 'C', 0.97171, '2021-10-04 09:07:21.291014');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (45, '2021-10-04 06:07:21.388284', 'C', 0.96806, '2021-10-04 09:07:21.399212');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (46, '2021-10-04 06:07:21.496280', 'B', 0.95595, '2021-10-04 09:07:21.510126');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (47, '2021-10-04 06:07:21.606290', 'A', 1.00739, '2021-10-04 09:07:21.618080');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (48, '2021-10-04 06:07:21.715393', 'B', 0.9552, '2021-10-04 09:07:23.730605');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (49, '2021-10-04 06:07:23.826673', 'B', 0.94796, '2021-10-04 09:07:23.836261');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (50, '2021-10-04 06:07:23.934062', 'C', 0.96454, '2021-10-04 09:07:23.944711');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (51, '2021-10-04 06:07:24.041626', 'A', 1.01049, '2021-10-04 09:07:24.054498');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (52, '2021-10-04 06:07:24.150732', 'A', 1.01102, '2021-10-04 09:07:24.162233');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (53, '2021-10-04 06:07:24.259118', 'B', 0.94305, '2021-10-04 09:07:24.269598');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (54, '2021-10-04 06:07:24.366998', 'A', 1.02673, '2021-10-04 09:07:24.378134');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (55, '2021-10-04 06:07:24.475250', 'B', 0.9329, '2021-10-04 09:07:28.488201');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (56, '2021-10-04 06:07:28.584383', 'A', 1.02618, '2021-10-04 09:07:28.595139');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (57, '2021-10-04 06:07:28.692484', 'C', 0.96724, '2021-10-04 09:07:28.702121');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (58, '2021-10-04 06:07:28.799815', 'C', 0.96101, '2021-10-04 09:07:28.811181');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (59, '2021-10-04 06:07:28.908667', 'C', 0.96141, '2021-10-04 09:07:28.920247');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (60, '2021-10-04 06:07:29.017096', 'A', 1.02838, '2021-10-04 09:07:29.028196');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (61, '2021-10-04 06:07:29.125187', 'A', 1.03289, '2021-10-04 09:07:29.134793');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (62, '2021-10-04 06:07:29.232494', 'B', 0.92813, '2021-10-04 09:07:29.243042');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (63, '2021-10-04 06:07:29.340384', 'A', 1.03808, '2021-10-04 09:07:29.351657');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (64, '2021-10-04 06:07:29.449231', 'B', 0.92843, '2021-10-04 09:07:29.460591');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (65, '2021-10-04 06:07:29.558429', 'A', 1.03386, '2021-10-04 09:07:32.575020');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (66, '2021-10-04 06:07:32.669387', 'C', 0.96844, '2021-10-04 09:07:34.681496');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (67, '2021-10-04 06:07:34.778482', 'C', 0.97214, '2021-10-04 09:07:34.787137');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (68, '2021-10-04 06:07:34.885284', 'A', 1.03154, '2021-10-04 09:07:34.894393');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (69, '2021-10-04 06:07:34.992493', 'A', 1.03264, '2021-10-04 09:07:35.002293');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (70, '2021-10-04 06:07:35.099687', 'B', 0.92057, '2021-10-04 09:07:35.109114');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (71, '2021-10-04 06:07:35.207180', 'B', 0.92487, '2021-10-04 09:07:35.216586');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (72, '2021-10-04 06:07:35.314277', 'C', 0.97067, '2021-10-04 09:07:35.322849');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (73, '2021-10-04 06:07:35.420665', 'C', 0.97461, '2021-10-04 09:07:35.429512');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (74, '2021-10-04 06:07:35.527564', 'B', 0.92373, '2021-10-04 09:07:35.537558');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (75, '2021-10-04 06:07:35.634628', 'B', 0.92639, '2021-10-04 09:07:35.641197');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (76, '2021-10-04 06:07:35.739684', 'A', 1.02947, '2021-10-04 09:07:35.748024');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (77, '2021-10-04 06:07:35.846287', 'B', 0.91911, '2021-10-04 09:07:35.854352');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (78, '2021-10-04 06:07:35.952511', 'B', 0.92687, '2021-10-04 09:07:35.961238');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (79, '2021-10-04 06:07:36.059335', 'C', 0.97565, '2021-10-04 09:07:36.070374');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (80, '2021-10-04 06:07:36.167353', 'A', 1.03757, '2021-10-04 09:07:39.183026');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (81, '2021-10-04 06:07:39.280248', 'A', 1.02917, '2021-10-04 09:07:39.291377');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (82, '2021-10-04 06:07:39.388476', 'A', 1.02972, '2021-10-04 09:07:39.397304');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (83, '2021-10-04 06:07:39.495249', 'C', 0.96991, '2021-10-04 09:07:39.507079');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (84, '2021-10-04 06:07:39.604343', 'C', 0.97698, '2021-10-04 09:07:39.612753');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (85, '2021-10-04 06:07:39.710603', 'A', 1.02748, '2021-10-04 09:07:39.720217');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (86, '2021-10-04 06:07:39.817590', 'B', 0.92767, '2021-10-04 09:07:39.829742');
INSERT INTO public.ticks (id, time, symbol, price, inserted) VALUES (87, '2021-10-04 06:07:39.926342', 'B', 0.92503, '2021-10-04 09:07:39.937227');