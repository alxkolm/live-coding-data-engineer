CREATE TABLE trades_enriched
(
    id       integer                                NOT NULL
        CONSTRAINT trades_enriched_pkey
            PRIMARY KEY,
    time     timestamp WITH TIME ZONE               NOT NULL,
    symbol   varchar(32)                            NOT NULL,
    price    double precision                       NOT NULL,
    inserted timestamp WITH TIME ZONE DEFAULT NOW() NOT NULL
);

ALTER TABLE trades_enriched
    OWNER TO postgres;

