FROM python:3.6

WORKDIR /service
COPY . ./

RUN pip install --no-cache-dir -r requirements.txt

ENTRYPOINT ["python"]
CMD ["--version"]
