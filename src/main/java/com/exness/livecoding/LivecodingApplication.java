package com.exness.livecoding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;

@SpringBootApplication
public class LivecodingApplication {

	public static void main(String[] args) {
		SpringApplication.run(LivecodingApplication.class, args);
	}
}
