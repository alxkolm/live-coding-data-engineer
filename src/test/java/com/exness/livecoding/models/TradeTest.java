package com.exness.livecoding.models;

import org.junit.jupiter.api.Test;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;

class TradeTest {
    @Test
    void deserialize() {
        JsonDeserializer<Trade> parser = new JsonDeserializer<>(Trade.class);

        var json = "{\"id\":4,\"time\":\"2021-09-22 08:17:01.611233\",\"symbol\":\"C\"}".getBytes(StandardCharsets.UTF_8);

        Trade trade = parser.deserialize("xxx", json);
        assertEquals(4, trade.getId());
        assertEquals(LocalDate.of(2021, 9, 22), trade.getTime().toLocalDate());
        assertEquals(LocalTime.of(8, 17, 1, 611233 * 1000), trade.getTime().toLocalTime());
        assertEquals("C", trade.getSymbol());
    }
}