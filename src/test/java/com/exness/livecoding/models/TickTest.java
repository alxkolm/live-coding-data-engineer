package com.exness.livecoding.models;

import org.junit.jupiter.api.Test;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;

class TickTest {
    @Test
    void deserialize() {
        JsonDeserializer<Tick> parser = new JsonDeserializer<>(Tick.class);

        var json = "{\"id\":1,\"time\":\"2021-09-22 07:51:52.014436\",\"symbol\":\"B\",\"price\":1.00171}".getBytes(StandardCharsets.UTF_8);

        Tick tick = parser.deserialize("xxx", json);
        assertEquals(1, tick.getId());
        assertEquals(LocalDate.of(2021, 9, 22), tick.getTime().toLocalDate());
        assertEquals(LocalTime.of(7, 51, 52, 14436 * 1000), tick.getTime().toLocalTime());
        assertEquals("B", tick.getSymbol());
        assertEquals(1.00171, tick.getPrice());
    }
}