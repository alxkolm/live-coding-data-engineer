package com.exness.livecoding;

import org.junit.ClassRule;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.images.builder.ImageFromDockerfile;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@Testcontainers
@SpringBootTest
@TestPropertySource(properties = {
        "spring.kafka.consumer.auto-offset-reset=earliest",
        "spring.datasource.url=jdbc:tc:postgresql:10:///databasename?TC_INITSCRIPT=init.sql&TC_DAEMON=true",

        // In case of using local db
        // "spring.datasource.url=jdbc:postgresql://localhost:5432/livecoding",
        // "spring.datasource.username=postgres",
        // "spring.datasource.password=mysecretpassword"
})
class EnvironmentSetupTest {
    protected static Logger log = LoggerFactory.getLogger(EnvironmentSetupTest.class);

    static private   Network      network  = Network.newNetwork();

    @Container
    public static KafkaContainer kafka = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:5.4.3"))
            .withNetwork(network)
            .withNetworkAliases("kafka");

    public static final ImageFromDockerfile PRODUCER_IMAGE = new ImageFromDockerfile()
            .withFileFromFile("Dockerfile", new File("Dockerfile"))
            .withFileFromPath("producer", new File("producer").toPath())
            .withFileFromFile("requirements.txt", new File("requirements.txt"));

    @ClassRule
    public static GenericContainer ticksProducer = new GenericContainer(PRODUCER_IMAGE)
            .withNetwork(network)
            .waitingFor(Wait.forLogMessage(".*Successfully connected to.*", 1));

    @ClassRule
    public static GenericContainer tradeProducer = new GenericContainer(PRODUCER_IMAGE)
            .withNetwork(network)
            .waitingFor(Wait.forLogMessage(".*Successfully connected to.*", 1));

    @Autowired
    private JdbcTemplate jdbc;

    protected static List<String> messages = new ArrayList<>();

    @BeforeAll
    public static void setup() {
        kafka.start();

        String bootstrapServers = "%s:%d".formatted("kafka", 9092);
        ticksProducer
                .withCommand("producer/ticks.py", bootstrapServers)
                .start();
        tradeProducer
                .withCommand("producer/trades.py", bootstrapServers)
                .start();
    }

    @Test
    void test() throws InterruptedException {
        Thread.sleep(5000);
        tradeProducer.stop();
        ticksProducer.stop();
        assertTrue(messages.size() > 1);
    }

    @DynamicPropertySource
    static void kafkaProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.kafka.consumer.group-id", () -> "live.coding.app");
        registry.add("spring.kafka.bootstrap-servers", () -> kafka.getBootstrapServers());
    }

    @TestConfiguration
    static class Configuration {
        @KafkaListener(topics = "ticks", groupId = "debug.consumer")
        public void ticksConsumer(@Payload String message) {
            log.info("Received tick: {}", message);
            messages.add(message);
        }

        @KafkaListener(topics = "trades", groupId = "debug.consumer")
        public void tradesConsumer(@Payload String message) {
            log.info("Received trade: {}", message);
            messages.add(message);
        }
    }
}