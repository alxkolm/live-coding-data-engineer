CREATE TABLE IF NOT EXISTS ticks
(
    id       INTEGER                  NOT NULL PRIMARY KEY,
    time     TIMESTAMP WITH TIME ZONE NOT NULL,
    symbol   VARCHAR(32)              NOT NULL,
    price    DOUBLE PRECISION         NOT NULL,
    inserted TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS trades
(
    id       INTEGER                  NOT NULL PRIMARY KEY,
    time     TIMESTAMP WITH TIME ZONE NOT NULL,
    symbol   VARCHAR(32)              NOT NULL,
    inserted TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS trades_enriched
(
    id       INTEGER                  NOT NULL PRIMARY KEY,
    time     TIMESTAMP WITH TIME ZONE NOT NULL,
    symbol   VARCHAR(32)              NOT NULL,
    price    DOUBLE PRECISION         NOT NULL,
    inserted TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
);
