# Live coding environment for Data Engineer position

### Для выполнения задания нужно:
1. Машина с установленным и рабочим Docker
2. JDK 16 + IDE для работы с Java-проектами
3. Gradle >= 7.1.1 (или можно использовать gradle-wrapper (`./gradlew`) из репозитория)
4. Клиент к базе PostgreSQL

Чтобы проверить, что среда работает, выполните:
```shell
gradle clean test --tests EnvironmentSetupTest --info
```

### Описание репозитория
1. Python-продюсер котировок и сделок [/producer](/producer)
   * Котировки отправляются в топик `ticks`
   * Сделки отправляются в топик `trades`
   * Участвует в интеграционных тестах
   * Нет необходимости кандидату в нём разбираться 
2. Java-проект [/src](/src)
   * unit- и интеграционные тесты [/src/test](/src/test)
      * [EnvironmentSetupTest](/src/test/java/com/exness/livecoding/EnvironmentSetupTest.java) интеграционый тест,
        для проверки того, что Kafka, python-продюсеры и java-код могут работать вместе.
   * [models](/src/main/java/com/exness/livecoding/models) POJO-классы для десериализации json

### Задание
Можно подготовится к livecoding заранее и сделать это как "домашнюю работу":
1. Дописать следующие классы `com.exness.livecoding.models.Tick`, 
   `com.exness.livecoding.models.Trade` для десериализации json,
   так что бы выполнялись тесты:
    ```shell
    gradle test --tests com.exness.livecoding.models.TickTest
    gradle test --tests com.exness.livecoding.models.TradeTest
    ```
   * Примеры json находятся в тестах, либо их можно увидеть в логах при запуске теста `EnvironmentSetupTest`
   * Класс должен реализовать getter'ы для всех полей
2. Написать kafka consumer, который принимает и десериализует json в POJO-объекты
   реализованные в п.1